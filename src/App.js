import React, { useState } from "react"
import axios from "axios"
import StateOptions from "./Components/StateOptions/StateOptions"
import Result from "./Components/Result/Result"
import './App.scss'

let App = () => {
  const [type, setType] = useState("")
  const [state, setState] = useState("")
  const [data, setData] = useState([])
  const [info, setInfo] = useState({ 
    firstName: "First Name", 
    lastName: "Last Name",
    district: "District",
    phone: "Phone",
    office: "Office",
    website: "Website"
  })

  let changeState = (state) => {
    setState(state)
  }

  let submit = async () => {
    try {
      if(state && type) {
        if(type === "representative") {
          let reps = await axios.get(`/api/representatives/${state}`)
          setData(reps.data.results)
        }
        else {
          let sens = await axios.get(`/api/senators/${state}`)
          setData(sens.data.results)
        }
      }
      else {
        alert("You must select Representative or Senator and a state")
      }
    }
    catch(err) {
      alert(err)
    }
  }

  let results = data.map((data, i) => <Result key={i} data={data} setInfo={setInfo} />)
  let infoClass = info.firstName === "First Name" ? "grey_text" : ""

  return (
    <div className="app">
      <header>
        <h1>Who is my representative?</h1>
      </header>
      <div className="form">
        <select className="select_type" name="type" onChange={(e) => setType(e.target.value)} defaultValue="type">
          <option className="select_placeholder" disabled="disabled" value="type">Find by Representative or Senator</option>
          <option value="representative">Representative</option>
          <option value="senator">Senator</option>
        </select>
        <StateOptions changeState={changeState} />
        <button onClick={() => submit()}>Submit</button>
      </div>
      <div className="data">
        <div className="title">
          <div className="list_rep">
            <h2>List / </h2>
            <h2>Representatives</h2>
          </div>
          <h2>Info</h2>
        </div>
        <div className="results_info">
        <div className="results">
          <div className="categories">
            <p>Name</p>
            <p>Party</p>
          </div>
            { results }
          </div>
          <div className="info">   
            <p className={infoClass}>{info.firstName}</p>
            <p className={infoClass}>{info.lastName}</p>
            <p className={infoClass}>{info.district}</p>
            <p className={infoClass}>{info.phone}</p>
            <p className={infoClass}>{info.office}</p>
            <p className={infoClass}>{info.website}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;