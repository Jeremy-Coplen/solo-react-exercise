import "./Result.scss"

let Result = (props) => {
    let setInfo = () => {
        props.setInfo({
            firstName: props.data.name.split(" ")[0],
            lastName: props.data.name.split(" ")[1],
            district: props.data.district,
            phone: props.data.phone,
            office: props.data.office,
            website: props.data.link
        })
    }
    return (
        <div className="result" onClick={setInfo}>
            <p className="name">{props.data.name}</p>
            <p>{props.data.party.toUpperCase().charAt(0)}</p>
        </div>
    )
}

export default Result